package com.m68476521.mike.popularmovies;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.m68476521.mike.popularmovies.utilities.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PopularMovieFragment extends Fragment {

    private final static String TAG = "PopularMovieFragment";
    private static final String SAVED_SUBTITLE_VISIBLE = "subtitle";
    private ArrayList<MovieItem> movieItems = new ArrayList<>();
    private MovieGridAdapter movieGridAdapter;
    private GridView gridview;
    private boolean mSubtitleVisible;
    private boolean showFavModel = false;

    private MovieLab movieLab;

    private boolean shouldSetAdapterFromSaveInstance = false;
    private ProgressBar progressBar;
    Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        movieLab = MovieLab.get(getActivity());
        mContext = getContext();

        if (savedInstanceState == null || !savedInstanceState.containsKey("movies")) {
            Log.d(TAG, "instanced null go to API");
            getPopularMovies();
        } else {
            Log.d(TAG, "instanced use previous model");

            movieItems = savedInstanceState.getParcelableArrayList("movies");
            shouldSetAdapterFromSaveInstance = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_view, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        gridview = view.findViewById(R.id.flavors_grid);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "ITEM_CLICKED" + showFavModel);
                MovieItem mMovieItem;
                if (!showFavModel) {
                    mMovieItem = movieItems.get(position);
                } else {
                    List<MovieItem> movies = movieLab.getMovies();
                    mMovieItem = movies.get(position);
                }

                Log.d(TAG, mMovieItem.getTitle());

                Intent intent = MovieDetailsActivity.newIntent(getContext(), mMovieItem.getId(),
                        mMovieItem.getTitle(), mMovieItem.getDescription(), mMovieItem.getPoster(),
                        mMovieItem.getReleaseDate(), mMovieItem.getVoteAverage(), mMovieItem.getOriginalLanguage());

                Context context = view.getContext();
                context.startActivity(intent);
            }
        });


        if (savedInstanceState != null) {
            mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }

        if (shouldSetAdapterFromSaveInstance) {
            setupAdapter();
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("movies", movieItems);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);

        MenuItem subtitleItem = menu.findItem(R.id.action_fav);
        if (mSubtitleVisible) {
            subtitleItem.setTitle(R.string.all_movies);
            showFavModel = true;
        } else {
            subtitleItem.setTitle(R.string.favorite_pivot);
            showFavModel = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemSelected = item.getItemId();

        mSubtitleVisible = !mSubtitleVisible;

        if (menuItemSelected == R.id.action_fav) {
            if (item.getTitle().equals("All")) {
                item.setTitle(R.string.favorite_pivot);
                setupAdapter();
                showFavModel = false;
            } else {
                item.setTitle(R.string.all_movies);
                setupAdapterFavorites();
                showFavModel = true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupAdapter() {
        movieGridAdapter = new MovieGridAdapter(getActivity(), movieItems);
        gridview.setAdapter(movieGridAdapter);
    }

    private void setupAdapterFavorites() {
        MovieLab movieLab = MovieLab.get(getActivity());
        ArrayList<MovieItem> movies = movieLab.getMovies();
        movieGridAdapter = new MovieGridAdapter(getActivity(), movies);
        gridview.setAdapter(movieGridAdapter);
        if (movies.size() == 0) {
            Toast toast = Toast.makeText(mContext, "You haven't added any movie to favorites", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void getPopularMovies() {
        URL searchUrl = NetworkUtils.buildUrl("popular", " ");
        new GetQueryTask().execute(searchUrl);
    }

    private class GetQueryTask extends AsyncTask<URL, Void, ArrayList<MovieItem>> {

        @Override
        protected ArrayList<MovieItem> doInBackground(URL... params) {
            try {
                NetworkUtils networkUtils = new NetworkUtils();
                movieItems = networkUtils.getPopularMovies();
                return movieItems;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return movieItems;
        }

        @Override
        protected void onPostExecute(ArrayList<MovieItem> newMovieItems) {
            if (movieItems != null && movieItems.size() > 0) {
                movieItems = newMovieItems;
                Log.d(TAG, " postExecute");
                progressBar.setVisibility(View.INVISIBLE);
                setupAdapter();
            }
        }
    }
}
