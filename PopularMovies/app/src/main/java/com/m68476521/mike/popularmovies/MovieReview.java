package com.m68476521.mike.popularmovies;

/**
 * This class is a bean for review, it is related to the movie
 */

public class MovieReview {
    private String author;
    private String review;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
